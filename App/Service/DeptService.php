<?php
/**
 * Note:     [Description]
 * Author:   longhui.huang <1592328848@qq.com>
 * DateTime: 2024/3/7 15:40
 */
declare(strict_types=1);

namespace App\Service;

use App\Dao\DeptDao;
use App\Model\DeptModel;

class DeptService extends BaseService
{
    /**
     * 根据角色ID查询
     *
     * @param int $id
     *
     * @return array|mixed
     */
    public function findByRoleId(int $id)
    {
        /** @var DeptDao $deptDao */
        $deptDao = app(DeptDao::class);
        return $deptDao->findByRoleId($id);
    }

    /**
     * 根据PID查询
     *
     * @param int $pid
     */
    public function findByPId(int $pid)
    {
        /** @var DeptDao $deptDao */
        $deptDao = app(DeptDao::class);
        return $deptDao->findByPId($pid);
    }

    /**
     * 获取
     *
     * @param DeptModel[] $deptList
     */
    public function getDeptChildren(array $deptList)
    {
        /** @var DeptDao $deptDao */
        $deptDao = app(DeptDao::class);
        $list    = [];

        foreach ($deptList as $dept) {
            if ($dept->enabled) {
                $depts = $deptDao->findByPId($dept->deptId);
                if ($depts) {
                    $children = $this->getDeptChildren($depts);
                    array_push($list, ...$children);
                }
                $list[] = $dept->deptId;
            }
        }

        return $list;
    }
}
