<?php
/**
 * Note:     [Description]
 * Author:   longhui.huang <1592328848@qq.com>
 * DateTime: 2024/3/7 21:49
 */
declare(strict_types=1);

namespace App\Service;

class DictDetailService extends BaseService
{
    public function queryAll(array $get)
    {
        $page     = intval($get['page'] ?? config('app.page-option.default-page'));
        $size     = intval($get['size'] ?? config('app.page-option.default-size'));
        $dictName = $get['dictName'] ?? '';

        return [];
    }
}
