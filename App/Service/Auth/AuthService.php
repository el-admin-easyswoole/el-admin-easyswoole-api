<?php
declare(strict_types=1);
/**
 * This file is part of EasySwoole.
 *
 * @link     https://www.easyswoole.com
 * @document https://www.easyswoole.com
 * @contact  https://www.easyswoole.com/Preface/contact.html
 * @license  https://github.com/easy-swoole/easyswoole/blob/3.x/LICENSE
 */

namespace App\Service\Auth;

use App\Exception\Api\BadRequestException;
use App\Model\UserModel;
use App\Service\BaseService;
use App\Service\Captcha\CaptchaService;
use App\Service\UserService;
use App\Utility\Rsa\RsaUtils;
use EasySwooleApi\Core\Request\Request;

class AuthService extends BaseService
{
    public function getCode()
    {
        /** @var CaptchaService $captchaService */
        $captchaService = app(CaptchaService::class);
        $result         = $captchaService->createCaptchaCode();
        return success($result);
    }

    public function login(array $post)
    {
        $username = $post['username'];
        $password = $post['password'];
        $code     = $post['code'];
        $uuid     = $post['uuid'];

        // 密码解密
        $privateKey    = config('app.rsa.private_key');
        $plainPassword = RsaUtils::decryptByPrivateKey($privateKey, $password);

        if (!$plainPassword) {
            throw new BadRequestException("验证码不存在或已过期");
        }

        // 检验验证码
        /** @var CaptchaService $captchaService */
        $captchaService = app(CaptchaService::class);
        $captchaService->checkCaptchaCode($uuid, $code);

        // 校验用户名或密码
        /** @var UserService $userService */
        $userService = app(UserService::class);
        /** @var UserModel $userModel */
        $userModel = $userService->findByUsername($username);

        if (!$userModel) {
            throw new BadRequestException("用户名或密码错误");
        }

        if (!password_verify($plainPassword, $userModel->password)) {
            throw new BadRequestException("用户名或密码错误");
        }

        if (!$userModel->enabled) {
            throw new BadRequestException("账号未激活!");
        }

        /** @var TokenService $tokenService */
        $tokenService = app(TokenService::class);
        $token        = $tokenService->createToken($username);

        /** @var UserDetailService $userDetailService */
        $userDetailService = app(UserDetailService::class);
        $jwtUserBean       = $userDetailService->loadUserByUsername($username);

        // 返回 token 与 用户信息
        $authInfo = [
            'user'  => $jwtUserBean,
            'token' => config('jwt.token-start-with') . $token,
        ];

        // 保存在线信息
        /** @var OnlineUserService $onlineUserService */
        $onlineUserService = app(OnlineUserService::class);
        $request           = request();
        $onlineUserService->save($jwtUserBean, $token, $request);

        return $authInfo;
    }

    public function logout(Request $request, string $username)
    {
        /** @var TokenService $tokenService */
        $tokenService = app(TokenService::class);
        $token        = $tokenService->getToken($request);
        /** @var OnlineUserService $onlineUserService */
        $onlineUserService = app(OnlineUserService::class);
        $onlineUserService->logout($username, $token);
    }
}
