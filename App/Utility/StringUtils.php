<?php
/**
 * Note:     [Description]
 * Author:   longhui.huang <1592328848@qq.com>
 * DateTime: 2024/3/7 11:58
 */
declare(strict_types=1);

namespace App\Utility;

use EasySwooleApi\Core\Request\Request;

class StringUtils
{
    public static function getBrowser(Request $request)
    {
        return $request->header('User-Agent');
    }
}
