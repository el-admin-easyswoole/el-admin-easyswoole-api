<?php
declare(strict_types=1);
/**
 * This file is part of EasySwoole.
 *
 * @link     https://www.easyswoole.com
 * @document https://www.easyswoole.com
 * @contact  https://www.easyswoole.com/Preface/contact.html
 * @license  https://github.com/easy-swoole/easyswoole/blob/3.x/LICENSE
 */

namespace App\Utility;

use Ramsey\Uuid\Uuid;

class IdUtil
{
    public static function simpleUUID()
    {
        $uuid = Uuid::uuid1();
        $uuidStr = $uuid->toString();
        return str_replace('-', '', $uuidStr);
    }
}
