<?php
declare(strict_types=1);
/**
 * This file is part of EasySwoole.
 *
 * @link     https://www.easyswoole.com
 * @document https://www.easyswoole.com
 * @contact  https://www.easyswoole.com/Preface/contact.html
 * @license  https://github.com/easy-swoole/easyswoole/blob/3.x/LICENSE
 */

namespace App\Utility\Redis;

use App\Utility\Redis\Exception\ExceptionTrait;

class RedisUtil
{
    public const DEFAULT_CONNECT = 'default';

    use ExceptionTrait;
    use SerializeTrait;

    use ConnectTrait;
    use KeyTrait;
    use StringTrait;
    use HashTrait;
    use ListTrait;
    use SetTrait;
    use SortedSetTrait;


    /**
     * 处理key前缀
     *
     * @param        $key
     * @param string $connectionName
     *
     * @return mixed|string
     */
    public static function handleKeyPrefix(&$key, string $connectionName = self::DEFAULT_CONNECT)
    {
        $prefix = config("redis.{$connectionName}.prefix");
        $prefix = $prefix ?: '';
        $key = $prefix . $key;
        return $key;
    }
}
