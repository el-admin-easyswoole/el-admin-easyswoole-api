<?php
/**
 * Note:     [Description]
 * Author:   longhui.huang <1592328848@qq.com>
 * DateTime: 2024/3/7 15:22
 */
declare(strict_types=1);

namespace App\Enums;

class DataScopeEnum
{
    public const  ALL        = 0;
    public const  THIS_LEVEL = 1;
    public const  CUSTOMIZE  = 2;

    private string $value;
    private string $description;
    private int    $enumId;

    public function __construct(string $value, string $description, int $enumId)
    {
        $this->value       = $value;
        $this->description = $description;
        $this->enumId      = $enumId;
    }

    public function getValue(): string
    {
        return $this->value;
    }

    public function setValue(string $value): void
    {
        $this->value = $value;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    public function getEnumId(): int
    {
        return $this->enumId;
    }

    public function setEnumId(int $enumId): void
    {
        $this->enumId = $enumId;
    }

    /* 全部的数据权限 */
    public static function ALL()
    {
        return new self('全部', '全部的数据权限', self::ALL);
    }

    /* 自己部门的数据权限 */
    public static function THIS_LEVEL()
    {
        return new self('本级', '自己部门的数据权限', self::THIS_LEVEL);
    }

    /* 自定义的数据权限 */
    public static function CUSTOMIZE()
    {
        return new self('自定义', '自定义的数据权限', self::CUSTOMIZE);
    }

    /**
     * @return DataScopeEnum[]
     */
    public static function values(): array
    {
        return [
            self::ALL(),
            self::THIS_LEVEL(),
            self::CUSTOMIZE()
        ];
    }

    public static function find(string $val = null): ?DataScopeEnum
    {
        if (!$val) {
            return null;
        }

        foreach (self::values() as $dataScopeEnum) {
            if ($dataScopeEnum->getValue() === $val) {
                return $dataScopeEnum;
            }
        }

        return null;
    }
}
