<?php
declare(strict_types=1);

namespace App\Exec\Command;

use App\Dao\RoleDao;
use App\Exception\Api\BadRequestException;
use App\Exception\Api\ModelNotFoundException;
use App\Model\UserModel;
use App\Service\Auth\UserDetailService;
use App\Service\DataService;
use App\Service\MenuService;
use App\Service\RoleService;
use App\Service\UserService;
use EasySwoole\Mysqli\Client;
use EasySwoole\Mysqli\Config;
use EasySwooleApi\Core\Exec\Command\BaseCommand;

class DemoCommand extends BaseCommand
{
    public function commandName(): string
    {
        return 'demo';
    }

    public function dropTable()
    {
//        $conf   = new Config([
//            'host'     => 'localhost',
//            'port'     => 3306,
//            'database' => 'el_admin_easyswoole',
//            'user'     => 'root',
//            'password' => 'woaiyouxi',
//            'charset'  => 'utf8mb4',
//            'timeout'  => 45,
//        ]);
//        $client = new Client($conf);
//        $client->queryBuilder()->raw("show tables");
//        $tables = $client->execBuilder();
//        foreach ($tables as $table) {
//            $table = $table['Tables_in_el_admin_easyswoole'];
//            $sql   = "drop table {$table}";
//            $client->queryBuilder()->raw($sql);
//            $client->execBuilder();
//        }
    }

    public function exec(): ?string
    {
        echo "this is demo.\n";

        go(function () {
            /** @var MenuService $menuService */
            $menuService = app(MenuService::class);
            $u = new UserModel(['userId' => 1]);
            $menuList    = $menuService->build($u);
            echo json_encode($menuList, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
            return;

            /** @var RoleDao $roleDao */
            $roleDao = app(RoleDao::class);
            $roleDao->findByUserId(1);
            return;

            //                        $this->dropTable();
            //                        return;

            $conf   = new Config([
                'host'     => 'localhost',
                'port'     => 3306,
                'database' => 'el_admin_easyswoole',
                'user'     => 'root',
                'password' => 'woaiyouxi',
                'charset'  => 'utf8mb4',
                'timeout'  => 45,
            ]);
            $client = new Client($conf);
            $client->queryBuilder()->raw("show tables");
            $tables = $client->execBuilder();
            //            var_dump($tables);

            $newColumnMap = [];
            foreach ($tables as $table) {
                $table = $table['Tables_in_el_admin_easyswoole'];
                $client->queryBuilder()->raw("show full columns from {$table}");
                $columns = $client->execBuilder();
                $columns = array_column($columns, 'Field');
                foreach ($columns as $column) {
                    $newColumn = convert_name($column, 1, false);
                    if ($newColumn !== $column) {
                        $newColumnMap[$column] = $newColumn;
                    }
                }
            }

//            $file = file_get_contents('/Users/xuesi/Downloads/el_admin_easyswoole.sql');
//            var_dump($newColumnMap);
//            foreach ($newColumnMap as $column => $newColumn) {
//                var_dump($column);
//                var_dump($newColumn);
//                $file = str_replace($column, $newColumn, $file);
//            }
//            file_put_contents(__DIR__.'/el_admin_easyswoole.sql', $file);
        });

        return parent::exec();
    }
}
