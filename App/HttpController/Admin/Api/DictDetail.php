<?php
/**
 * Note:     [Description]
 * Author:   longhui.huang <1592328848@qq.com>
 * DateTime: 2024/3/7 21:46
 */
declare(strict_types=1);

namespace App\HttpController\Admin\Api;

use App\Exception\Api\BadRequestException;
use App\HttpController\Admin\AdminBase;
use App\Service\Auth\AuthService;
use App\Service\DictDetailService;

class DictDetail extends AdminBase
{
    /** @var DictDetailService|null */
    protected $dictDetailService;

    protected function onRequest(?string $action): ?bool
    {
        $this->dictDetailService = app(DictDetailService::class);
        return parent::onRequest($action);
    }

    public function queryDictDetail()
    {
        try {
            $list = $this->dictDetailService->queryAll($this->_request->get());
        } catch (BadRequestException $badRequestException) {
            return fail($badRequestException->getMessage());
        }

        return success($list);
    }
}
