<?php
declare(strict_types=1);
/**
 * This file is part of EasySwoole.
 *
 * @link     https://www.easyswoole.com
 * @document https://www.easyswoole.com
 * @contact  https://www.easyswoole.com/Preface/contact.html
 * @license  https://github.com/easy-swoole/easyswoole/blob/3.x/LICENSE
 */

namespace App\HttpController\Admin\Api;

use App\Exception\Api\BadRequestException;
use App\HttpController\Admin\AdminBase;
use App\Service\MenuService;

class Menus extends AdminBase
{
    /**
     * 获取前端所需菜单
     *
     * @return false|mixed
     */
    public function build()
    {
        try {
            /** @var MenuService $menuService */
            $menuService = app(MenuService::class);
            $menuList    = $menuService->build($this->user);
        } catch (BadRequestException $badRequestException) {
            return fail($badRequestException->getMessage());
        }

        return success($menuList);
    }
}
