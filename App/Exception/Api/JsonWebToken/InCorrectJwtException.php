<?php
/**
 * Note:     [Description]
 * Author:   longhui.huang <1592328848@qq.com>
 * DateTime: 2024/3/7 11:41
 */
declare(strict_types=1);

namespace App\Exception\Api\JsonWebToken;

use EasySwoole\Jwt\Exception;

class InCorrectJwtException extends Exception
{

}
