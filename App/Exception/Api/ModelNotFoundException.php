<?php
/**
 * Note:     [Description]
 * Author:   longhui.huang <1592328848@qq.com>
 * DateTime: 2024/3/7 14:47
 */
declare(strict_types=1);

namespace App\Exception\Api;

class ModelNotFoundException extends \RuntimeException
{
    public function __construct(string $class, string $field, string $val)
    {
        $message = $this->generateMessage($class, $field, $val);
        parent::__construct($message);
    }

    private function generateMessage(string $model, string $field, string $val)
    {
        return $model . " with {$field} {$val} does not exist";
    }
}
