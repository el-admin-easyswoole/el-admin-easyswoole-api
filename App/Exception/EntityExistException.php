<?php
/**
 * Note:     [Description]
 * Author:   longhui.huang <1592328848@qq.com>
 * DateTime: 2024/2/7 16:34
 */
declare(strict_types=1);

namespace App\Exception;

use EasySwoole\Http\Message\Status;
use Exception;

class EntityExistException extends Exception
{
    public function __construct(object $obj, string $field, $value)
    {
        $message = "the record has existed";
        $code    = Status::CODE_BAD_REQUEST;
        parent::__construct($message, $code);
    }
}
