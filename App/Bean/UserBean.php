<?php
/**
 * Note:     [Description]
 * Author:   longhui.huang <1592328848@qq.com>
 * DateTime: 2024/2/7 16:49
 */
declare(strict_types=1);

namespace App\Bean;

class UserBean extends BaseBean
{
    protected ?int      $userId;
    protected ?string   $username;
    protected ?string   $email;
    protected ?string   $phone;
    protected ?DeptBean $dept;
    protected ?int      $deptId;

    public function getUserId(): int
    {
        return $this->userId;
    }

    public function setUserId(int $userId): void
    {
        $this->userId = $userId;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(?string $username): void
    {
        $this->username = $username;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): void
    {
        $this->email = $email;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): void
    {
        $this->phone = $phone;
    }

    public function getDept(): ?DeptBean
    {
        return $this->dept;
    }

    public function setDept(?DeptBean $dept): void
    {
        $this->dept = $dept;
    }

    public function getDeptId(): ?int
    {
        return $this->deptId;
    }

    public function setDeptId(?int $deptId): void
    {
        $this->deptId = $deptId;
    }
}
