<?php
/**
 * Note:     [Description]
 * Author:   longhui.huang <1592328848@qq.com>
 * DateTime: 2024/3/7 20:54
 */
declare(strict_types=1);

namespace App\Bean;

class MenuMetaVoBean extends BaseBean
{
    protected string $title;
    protected string $icon;
    protected bool   $noCache;

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    public function getIcon(): string
    {
        return $this->icon;
    }

    public function setIcon(string $icon): void
    {
        $this->icon = $icon;
    }

    public function isNoCache(): bool
    {
        return $this->noCache;
    }

    public function setNoCache(bool $noCache): void
    {
        $this->noCache = $noCache;
    }
}
