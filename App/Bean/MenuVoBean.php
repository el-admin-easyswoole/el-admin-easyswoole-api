<?php
/**
 * Note:     [Description]
 * Author:   longhui.huang <1592328848@qq.com>
 * DateTime: 2024/3/7 20:52
 */
declare(strict_types=1);

namespace App\Bean;

class MenuVoBean extends BaseBean
{
    protected ?string        $name;
    protected string         $path;
    protected bool           $hidden;
    protected string         $redirect;
    protected ?string         $component;
    protected bool           $alwaysShow;
    protected ?MenuMetaVoBean $meta;
    /**
     * @var MenuVoBean[]
     */
    protected array $children;

    public function getName(): string
    {
        return $this->name;
    }

    public function setName($name): void
    {
        $this->name = $name;
    }

    public function getPath(): string
    {
        return $this->path;
    }

    public function setPath(string $path): void
    {
        $this->path = $path;
    }

    public function isHidden(): bool
    {
        return $this->hidden;
    }

    public function setHidden(bool $hidden): void
    {
        $this->hidden = $hidden;
    }

    public function getRedirect(): string
    {
        return $this->redirect;
    }

    public function setRedirect(string $redirect): void
    {
        $this->redirect = $redirect;
    }

    public function getComponent(): string
    {
        return $this->component;
    }

    public function setComponent($component): void
    {
        $this->component = $component;
    }

    public function isAlwaysShow(): bool
    {
        return $this->alwaysShow;
    }

    public function setAlwaysShow(bool $alwaysShow): void
    {
        $this->alwaysShow = $alwaysShow;
    }

    public function getMeta(): MenuMetaVoBean
    {
        return $this->meta;
    }

    public function setMeta($meta): void
    {
        $this->meta = $meta;
    }

    public function getChildren(): array
    {
        return $this->children;
    }

    public function setChildren(array $children): void
    {
        $this->children = $children;
    }
}
