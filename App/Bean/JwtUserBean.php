<?php
/**
 * Note:     [Description]
 * Author:   longhui.huang <1592328848@qq.com>
 * DateTime: 2024/3/7 11:54
 */
declare(strict_types=1);

namespace App\Bean;

use App\Model\UserModel;

class JwtUserBean extends BaseBean
{
    protected UserModel $user;
    protected array     $dataScopes;
    protected array     $authorities;
    protected array     $roles;

    public function getUser(): UserModel
    {
        return $this->user;
    }

    public function setUser(?UserModel $user): void
    {
        $this->user = $user;
    }

    public function getUsername()
    {
        return $this->user->username;
    }

    public function setDataScopes(array $dataScopes): void
    {
        $this->dataScopes = $dataScopes;
    }

    public function setAuthorities(array $authorities): void
    {
        $this->authorities = $authorities;
    }

    public function setRoles(array $roles): void
    {
        $this->roles = $roles;
    }
}
