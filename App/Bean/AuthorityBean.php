<?php
/**
 * Note:     [Description]
 * Author:   longhui.huang <1592328848@qq.com>
 * DateTime: 2024/3/7 16:06
 */
declare(strict_types=1);

namespace App\Bean;

class AuthorityBean extends BaseBean
{
    protected string $authority;

    public function getAuthority(): string
    {
        return $this->authority;
    }

    public function setAuthority(string $authority): void
    {
        $this->authority = $authority;
    }
}
