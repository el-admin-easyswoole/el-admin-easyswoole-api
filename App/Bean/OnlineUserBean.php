<?php
/**
 * Note:     [Description]
 * Author:   longhui.huang <1592328848@qq.com>
 * DateTime: 2024/3/7 11:25
 */
declare(strict_types=1);

namespace App\Bean;

class OnlineUserBean extends BaseBean
{
    /**
     * @var string 用户名
     */
    protected string $username;

    /**
     * @var string 昵称
     */
    protected string $nickname;

    /**
     * @var string 岗位
     */
    protected string $dept;

    /**
     * @var string 浏览器
     */
    protected string $browser;

    /**
     * @var string IP
     */
    protected string $ip;

    /**
     * @var string 地址
     */
    protected string $address;

    /**
     * @var string token
     */
    protected string $key;

    /**
     * @var int 登录时间
     */
    protected int $loginTime;

    public function getUsername(): string
    {
        return $this->username;
    }

    public function setUsername(string $username): void
    {
        $this->username = $username;
    }

    public function getNickname(): string
    {
        return $this->nickname;
    }

    public function setNickname(string $nickname): void
    {
        $this->nickname = $nickname;
    }

    public function getDept(): string
    {
        return $this->dept;
    }

    public function setDept(string $dept): void
    {
        $this->dept = $dept;
    }

    public function getBrowser(): string
    {
        return $this->browser;
    }

    public function setBrowser(string $browser): void
    {
        $this->browser = $browser;
    }

    public function getIp(): string
    {
        return $this->ip;
    }

    public function setIp(string $ip): void
    {
        $this->ip = $ip;
    }

    public function getAddress(): string
    {
        return $this->address;
    }

    public function setAddress(string $address): void
    {
        $this->address = $address;
    }

    public function getKey(): string
    {
        return $this->key;
    }

    public function setKey(string $key): void
    {
        $this->key = $key;
    }

    public function getLoginTime(): int
    {
        return $this->loginTime;
    }

    public function setLoginTime(int $loginTime): void
    {
        $this->loginTime = $loginTime;
    }
}
