<?php
/**
 * Note:     [Description]
 * Author:   longhui.huang <1592328848@qq.com>
 * DateTime: 2024/2/7 16:50
 */
declare(strict_types=1);


namespace App\Bean;

use EasySwoole\Spl\SplBean;

abstract class BaseBean extends SplBean
{

}
