<?php
/**
 * Note:     [Description]
 * Author:   longhui.huang <1592328848@qq.com>
 * DateTime: 2024/3/7 21:41
 */
declare(strict_types=1);

namespace App\Router\Admin;

use EasySwooleApi\Core\Router\IRouterInterface;
use FastRoute\RouteCollector;

class ApiRouter implements IRouterInterface
{
    public function register(RouteCollector &$routeCollector): void
    {
        $routeCollector->addGroup('/auth', function (RouteCollector $routeCollector) {
            $routeCollector->addRoute('GET', '/code', '/Admin/Auth/code');
            $routeCollector->addRoute('GET', '/info', '/Admin/Auth/info');
            $routeCollector->addRoute('POST', '/login', '/Admin/Auth/login');
            $routeCollector->addRoute('DELETE', '/logout', '/Admin/Auth/logout');
        });

        $routeCollector->addGroup('/api', function (RouteCollector $routeCollector) {
            // menu
            $routeCollector->addRoute('GET', '/menus', '/Admin/Api/Menus/build'); // todo::
            $routeCollector->addGroup('/menus', function (RouteCollector $routeCollector) {
                $routeCollector->addRoute('GET', '/build', '/Admin/Api/Menus/build');
                $routeCollector->addRoute('GET', '/lazy', '/Admin/Api/Menus/build'); // todo::
            });

            $routeCollector->addRoute('GET', '/dictDetail', '/Admin/Api/DictDetail/queryDictDetail'); // todo::
            $routeCollector->addRoute('GET', '/users', '/Admin/Api/Menus/build'); // todo::
            $routeCollector->addRoute('GET', '/dept', '/Admin/Api/Menus/build'); // todo::
            $routeCollector->addRoute('GET', '/roles', '/Admin/Api/Menus/build'); // todo::
            $routeCollector->addGroup('/roles', function (RouteCollector $routeCollector) {
                $routeCollector->addRoute('GET', '/level', '/Admin/Api/Menus/build'); // todo::
                $routeCollector->addRoute('GET', '/lazy', '/Admin/Api/Menus/build'); // todo::
            });

            $routeCollector->addRoute('GET', '/job', '/Admin/Api/Menus/build'); // todo::
            $routeCollector->addRoute('GET', '/dict', '/Admin/Api/Menus/build'); // todo::
        });
    }
}
