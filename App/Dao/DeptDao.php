<?php
/**
 * Note:     [Description]
 * Author:   longhui.huang <1592328848@qq.com>
 * DateTime: 2024/3/7 15:42
 */
declare(strict_types=1);

namespace App\Dao;

use App\Model\DeptModel;
use EasySwoole\Mysqli\QueryBuilder;
use EasySwoole\ORM\DbManager;

class DeptDao extends BaseDao
{
    protected function setModel(): string
    {
        return DeptModel::class;
    }

    public function findById(int $deptId): ?DeptModel
    {
        return $this->getOne(['deptId' => $deptId]);
    }

    public function findByRoleId(int $id)
    {
        $fields = 'd.deptId as id, d.name, d.pid, d.subCount, d.createTime, d.updateTime, d.createBy, d.updateBy, d.enabled, d.deptSort';

        $builder = new QueryBuilder();
        $builder->fields($fields)
                ->where('d.deptId', 'r.deptId')
                ->where('r.roleId', $id)
                ->get('sys_dept d, sys_roles_depts r');

        $res = DbManager::getInstance()->query($builder, true)->getResult();
        return $res ?: [];
    }

    /**
     * 根据PID查询
     *
     * @param int $pid
     */
    public function findByPId(int $pid)
    {
        $where = ['pid' => $pid];
        $field = 'deptId, name, pid, subCount, createTime, updateTime, createBy, updateBy, enabled, deptSort';
        return $this->selectList($where, $field);
    }
}
