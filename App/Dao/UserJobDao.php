<?php
/**
 * Note:     [Description]
 * Author:   longhui.huang <1592328848@qq.com>
 * DateTime: 2024/2/7 17:02
 */
declare(strict_types=1);

namespace App\Dao;

use App\Model\UserJobModel;

class UserJobDao extends BaseDao
{
    protected function setModel(): string
    {
        return UserJobModel::class;
    }

    public function findByUserId(int $userId)
    {
        /** @var UserJobModel $list */
        $list   = $this->selectList(['userId' => $userId]);
        $jobIds = [];
        foreach ($list as $item) {
            $jobIds[] = $item->jobId;
        }

        if ($jobIds) {
            /** @var JobDao $jobDao */
            $jobDao = app(JobDao::class);
            return $jobDao->selectList(['jobId' => [$jobIds, 'IN']]);
        }

        return [];
    }
}
