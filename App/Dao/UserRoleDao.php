<?php
/**
 * Note:     [Description]
 * Author:   longhui.huang <1592328848@qq.com>
 * DateTime: 2024/3/7 17:48
 */
declare(strict_types=1);

namespace App\Dao;

use App\Model\UserRoleModel;

class UserRoleDao extends BaseDao
{
    protected function setModel(): string
    {
        return UserRoleModel::class;
    }

    public function findByUserId(int $userId)
    {
        /** @var UserRoleModel $list */
        $list    = $this->selectList(['userId' => $userId]);
        $roleIds = [];
        foreach ($list as $item) {
            $roleIds[] = $item->roleId;
        }

        if ($roleIds) {
            /** @var RoleDao $roleDao */
            $roleDao = app(RoleDao::class);
            return $roleDao->selectList(['roleId' => [$roleIds, 'IN']]);
        }

        return [];
    }
}
