<?php
/**
 * Note:     [Description]
 * Author:   longhui.huang <1592328848@qq.com>
 * DateTime: 2024/3/7 17:47
 */
declare(strict_types=1);

namespace App\Dao;

use App\Model\JobModel;

class JobDao extends BaseDao
{
    protected function setModel(): string
    {
        return JobModel::class;
    }
}
