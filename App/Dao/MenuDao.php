<?php
/**
 * Note:     [Description]
 * Author:   longhui.huang <1592328848@qq.com>
 * DateTime: 2024/3/7 20:34
 */
declare(strict_types=1);

namespace App\Dao;

use App\Model\MenuModel;

class MenuDao extends BaseDao
{
    protected function setModel(): string
    {
        return MenuModel::class;
    }

    public function findByRoleIdsAndTypeNot(array $roleIds, int $type)
    {
        $where = [
            'r.roleId' => [$roleIds, 'IN'],
            'type'     => [$type, '!='],
        ];
        $field = 'm.*';
        $joins = [
            ['sys_roles_menus r', 'm.menuId = r.menuId', 'INNER']
        ];
        $order = ['m.menuSort'];
        return $this->selectList($where, $field, $order, 'm', $joins);
    }
}
