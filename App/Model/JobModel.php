<?php
/**
 * Note:     [Description]
 * Author:   longhui.huang <1592328848@qq.com>
 * DateTime: 2024/3/7 10:06
 */
declare(strict_types=1);

namespace App\Model;

/**
 * Class JobModel
 *
 * @package App\Model
 * @property int         $jobId
 * @property string      $name
 * @property string      $enabled
 * @property int|null    $jobSort
 * @property string|null $createBy
 * @property string|null $updateBy
 * @property string|null $createTime
 * @property string|null $updateTime
 */
class JobModel extends BaseModel
{
    protected $tableName = 'sys_job';

    public function getEnabledAttr($value)
    {
        return boolval(ord($value));
    }
}
