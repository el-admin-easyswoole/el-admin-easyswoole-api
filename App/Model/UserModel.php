<?php
declare(strict_types=1);

/**
 * This file is part of EasySwoole.
 *
 * @link     https://www.easyswoole.com
 * @document https://www.easyswoole.com
 * @contact  https://www.easyswoole.com/Preface/contact.html
 * @license  https://github.com/easy-swoole/easyswoole/blob/3.x/LICENSE
 */

namespace App\Model;

/**
 * Class SysUserModel
 *
 * @package App\Model
 * @property int         $userId
 * @property int|null    $deptId
 * @property string|null $username
 * @property string|null $nickName
 * @property string|null $gender
 * @property string|null $phone
 * @property string|null $email
 * @property string|null $avatarName
 * @property string|null $avatarPath
 * @property string|null $password
 * @property string|null $isAdmin
 * @property string|null $enabled
 * @property string|null $createBy
 * @property string|null $updateBy
 * @property string|null $pwdResetTime
 * @property string|null $createTime
 * @property string|null $updateTime
 */
class UserModel extends BaseModel
{
    protected $tableName = 'sys_user';

    public function getIsAdminAttr($value)
    {
        if (is_bool($value)) {
            return $value;
        }

        return boolval(ord($value));
    }

    public function getEnabledAttr($value)
    {
        if (is_bool($value)) {
            return $value;
        }

        return boolval(ord($value));
    }
}
