<?php
/**
 * Note:     [Description]
 * Author:   longhui.huang <1592328848@qq.com>
 * DateTime: 2024/2/1 13:03
 */
declare(strict_types=1);

namespace App\Model;

/**
 * Class DeptModel
 *
 * @package App\Model
 * @property int         $deptId
 * @property int|null    $pid
 * @property int|null    $subCount
 * @property string      $name
 * @property int|null    $deptSort
 * @property string      $enabled
 * @property string|null $createBy
 * @property string|null $updateBy
 * @property string|null $createTime
 * @property string|null $updateTime
 */
class DeptModel extends BaseModel
{
    protected $tableName = 'sys_dept';

    public function getEnabledAttr($value)
    {
        return boolval(ord($value));
    }
}
