<?php
/**
 * Note:     [Description]
 * Author:   longhui.huang <1592328848@qq.com>
 * DateTime: 2024/3/7 10:06
 */
declare(strict_types=1);

namespace App\Model;

/**
 * Class MenuModel
 *
 * @package App\Model
 * @property int         $menuId
 * @property int|null    $pid
 * @property int|null    $subCount
 * @property int|null    $type
 * @property string|null $title
 * @property string|null $name
 * @property string|null $component
 * @property int|null    $menuSort
 * @property string|null $icon
 * @property string|null $path
 * @property string|null $iFrame
 * @property string|null $cache
 * @property bool|null $hidden
 * @property string|null $permission
 * @property string|null $createBy
 * @property string|null $updateBy
 * @property string|null $createTime
 * @property string|null $updateTime
 * @property MenuModel[]|null $children
 */
class MenuModel extends BaseModel
{
    protected $tableName = 'sys_menu';

    public function getIFrameAttr($value)
    {
        return boolval(ord($value));
    }

    public function getCacheAttr($value)
    {
        return boolval(ord($value));
    }

    public function getHiddenAttr($value)
    {
        return boolval(ord($value));
    }
}
