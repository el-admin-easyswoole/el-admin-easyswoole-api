<?php
/**
 * Note:     [Description]
 * Author:   longhui.huang <1592328848@qq.com>
 * DateTime: 2024/3/7 17:44
 */
declare(strict_types=1);

namespace App\Model;

class UserJobModel extends BaseModel
{
    protected $tableName = 'sys_users_jobs';
}
