<?php
declare(strict_types=1);
/**
 * This file is part of EasySwoole.
 *
 * @link     https://www.easyswoole.com
 * @document https://www.easyswoole.com
 * @contact  https://www.easyswoole.com/Preface/contact.html
 * @license  https://github.com/easy-swoole/easyswoole/blob/3.x/LICENSE
 */

namespace App\Model;

use EasySwoole\ORM\AbstractModel;

class BaseModel extends AbstractModel
{
    public function orders(...$args)
    {
        $orders = array_shift($args);

        if ($orders) {
            if (is_string($orders)) {
                $orderArr = explode(',', $orders);
                foreach ($orderArr as $item) {
                    $temp = explode(' ', $item);
                    parent::order($temp[0], $temp[1]);
                }
            } else if (is_array($orders)) {
                foreach ($orders as $field => $order) {
                    if (is_numeric($field)) {
                        $field = $order;
                        $order = 'ASC';
                    }
                    parent::order($field, $order);
                }
            }
        }

        return $this;
    }
}
