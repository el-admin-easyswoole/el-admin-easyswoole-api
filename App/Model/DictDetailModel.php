<?php
/**
 * Note:     [Description]
 * Author:   longhui.huang <1592328848@qq.com>
 * DateTime: 2024/3/7 10:05
 */
declare(strict_types=1);

namespace App\Model;

/**
 * Class DictDetailModel
 *
 * @package App\Model
 * @property int         $detailId
 * @property int|null    $dictId
 * @property string      $label
 * @property string      $value
 * @property int|null    $dictSort
 * @property string|null $createBy
 * @property string|null $updateBy
 * @property string|null $createTime
 * @property string|null $updateTime
 */
class DictDetailModel extends BaseModel
{
    protected $tableName = 'sys_dict_detail';
}
