<?php
/**
 * Note:     [Description]
 * Author:   longhui.huang <1592328848@qq.com>
 * DateTime: 2024/3/7 10:04
 */
declare(strict_types=1);

namespace App\Model;

/**
 * Class DictModel
 *
 * @package App\Model
 * @property int         $dictId
 * @property string      $name
 * @property string|null $description
 * @property string|null $createBy
 * @property string|null $updateBy
 * @property string|null $createTime
 * @property string|null $updateTime
 */
class DictModel extends BaseModel
{
    protected $tableName = 'sys_dict';
}
