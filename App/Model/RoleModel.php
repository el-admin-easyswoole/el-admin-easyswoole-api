<?php
/**
 * Note:     [Description]
 * Author:   longhui.huang <1592328848@qq.com>
 * DateTime: 2024/3/7 10:07
 */
declare(strict_types=1);

namespace App\Model;

/**
 * Class RoleModel
 *
 * @package App\Model
 * @property int         $roleId
 * @property string      $name
 * @property int|null    $level
 * @property string|null $description
 * @property string|null $dataScope
 * @property string|null $createBy
 * @property string|null $updateBy
 * @property string|null $createTime
 * @property string|null $updateTime
 */
class RoleModel extends BaseModel
{
    protected $tableName = 'sys_role';
}
