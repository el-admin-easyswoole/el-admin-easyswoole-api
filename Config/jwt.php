<?php
declare(strict_types=1);
// jwt配置
return [
    'header'                    => 'Authorization',
    'token-start-with'          => 'Bearer ', # 令牌前缀
    'alg'                       => 'HMACSHA256',
    'base64-secret'             => 'fd4db9644040cb8231cf7fb727a7ff23a85b985da450c0c840976127c9c0adfe0ef9a4f7e88ce7a1585dd59cf78f0ea57535d6b1cd744c1ee62d726572f51432', # 必须使用最少88位的Base64对该令牌进行编码
    'token-validity-in-seconds' => 14400000, # 令牌过期时间 此处单位/毫秒 ，默认4小时，可在此网站生成 https://www.convertworld.com/zh-hans/time/milliseconds.html
    'iss'                       => 'el_admin_easyswoole',
    'online-key'                => 'online-token:', # 在线用户key
    'detect'                    => 1800000, # token 续期检查时间范围（默认30分钟，单位毫秒），在token即将过期的一段时间内用户操作了，则给用户的token续期
    'renew'                     => 3600000, # 续期时间范围，默认1小时，单位毫秒
];
