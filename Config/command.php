<?php
declare(strict_types=1);
return [
    \App\Exec\Command\DemoCommand::class,
    \App\Exec\Command\GenBean\ModelCommand::class,
];
