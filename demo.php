<?php
/**
 * Note:     [Description]
 * Author:   longhui.huang <1592328848@qq.com>
 * DateTime: 2024/1/30 14:42
 */
declare(strict_types=1);
require_once getcwd().'/vendor/autoload.php';
//\App\Utility\Rsa\RsaUtils::main();
function parseName($name, $type = 0, $ucfirst = true)
{
    if ($type) {
        $name = preg_replace_callback('/_([a-zA-Z])/', function ($match) {
            return strtoupper($match[1]);
        }, $name);
        return $ucfirst ? ucfirst($name) : lcfirst($name);
    }

    return strtolower(trim(preg_replace("/[A-Z]/", "_\\0", $name), "_"));
}
$k = 'dept_id';
$r = parseName($k, 1, false);var_dump($r);
